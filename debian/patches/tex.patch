Description: Fixes/workarounds to issues breaking pdflatex compilation
 which causes FTBFS 
Author: Alastair McKinstry <mckinstry@debian.org>
Last-Updated: 2020-12-09
Forwarded: no

Index: python-escript-5.6/doc/user/linearPDE.tex
===================================================================
--- python-escript-5.6.orig/doc/user/linearPDE.tex
+++ python-escript-5.6/doc/user/linearPDE.tex
@@ -76,7 +76,7 @@ The coefficient $d$ is a \RankTwo and $y
 u_{i}=r_{i} \mbox{ where } q_{i}>0
 \end{equation}
 $r$ and $q$ are each a \RankOne. Notice that not necessarily all components
-must have a constraint at all locations.  An example for a system of PDEs is shown in the elastic deformation example in section \Refe{ELASTIC CHAP}
+must have a constraint at all locations.  An example for a system of PDEs is shown in the elastic deformation example %in section \Refe{ELASTIC CHAP}
 
 \LinearPDE also supports solution discontinuities\index{discontinuity} over a
 contact region $\Gamma^{contact}$ in the domain $\Omega$.
@@ -178,7 +178,7 @@ object gives access to diagnostic inform
       mypde.getDiagnostics('residual_norm'))
 \end{python}
 Typically, a negative value for a diagnostic variable indicates that it is
-undefined.  For more details on \SolverOptions and Trilinos see chapter \Refe{TRILINOS}.
+undefined.  For more details on \SolverOptions and Trilinos see chapter \Refe{Trilinos}.
 
 \subsection{Classes}
 %\declaremodule{extension}{esys.escript.linearPDEs} 
@@ -419,7 +419,7 @@ as discussed in \Chap{ESCRIPT CHAP}. If
 \begin{equation}\label{PROJ.1}
 u = Y
 \end{equation}
-of the general scalar PDE~\Refe{LINEARPDE.SINGLE.1} (boundary conditions are
+of the general scalar PDE (boundary conditions are
 irrelevant), you can see the solution $u$ of this PDE as a projection of the
 input function $Y$ which has the \Function attribute to a function with the
 \SolutionFS or \ReducedSolutionFS attribute.
@@ -450,7 +450,7 @@ to a function with the \ReducedSolutionF
   # alternative call:
   u=proj(jmp)
 \end{python}
-By default the class uses lumping to solve the PDE~\Refe{PROJ.1}.
+By default the class uses lumping to solve the PDE.
 This technique is faster than using the standard solver techniques of PDEs.
 In essence it leads to using the average of neighbour element values to
 calculate the value at each FEM node. 
@@ -527,9 +527,9 @@ The value of \var{method} must be one of
 %\member{Solver.Options.DIRECT_MUMPS}--runtime error\\
 %\member{SolverOptions.DIRECT_PARDISO}--runtime error\\
  \member{SolverOptions.DIRECT_SUPERLU} -- use a direct SUPERLU solver\\
- \member{SolverOptions.DIRECT_TRILINOS} -- use default TRILINOS default solver (KLU2) (chapter \Refe{TRILINOS})\\  
+ \member{SolverOptions.DIRECT_TRILINOS} -- use default TRILINOS default solver (KLU2) (chapter \Refe{Trilinos})\\  
  \member{SolverOptions.GMRES} -- Gram-Schmidt minimum residual method\\
- \member{SolverOptions.HRZ_LUMPING} -- Matrix lumping using the HRZ approach (section \Refe{WAVE CHAP})\\
+ \member{SolverOptions.HRZ_LUMPING} -- Matrix lumping using the HRZ approach \\ % (section  \Refe{WAVE CHAP})\\
  \member{SolverOptions.ITERATIVE} -- use a suitable iterative solver\\
  \member{SolverOptions.LSQR} -- Least squares based LSQR solver\\
  \member{SolverOptions.LUMPING} -- Matrix lumping\\
@@ -541,7 +541,7 @@ The value of \var{method} must be one of
  \member{SolverOptions.TFQMR} -- Transpose Free Quasi Minimum Residual method.\\
 Not all packages support all solvers. It can be assumed that a package makes a
 reasonable choice if it encounters an unknown solver.
-See Table~\Refe{TAB FINLEY SOLVER OPTIONS 1} for the solvers supported by
+% See Table~\Refe{TAB FINLEY SOLVER OPTIONS 1} for the solvers supported by
 \finley.
 \end{methoddesc}
 
@@ -563,7 +563,7 @@ The value of \var{preconditioner} must b
  \member{SolverOptions.RILU} -- relaxed ILU0.\\
 Not all packages support all preconditioners. It can be assumed that a package
 makes a reasonable choice if it encounters an unknown preconditioner.
-See Table~\Refe{TAB FINLEY SOLVER OPTIONS 2} for the preconditioners supported
+% See Table~\Refe{TAB FINLEY SOLVER OPTIONS 2} for the preconditioners supported
 by \finley.
 \end{methoddesc}
    
@@ -578,7 +578,7 @@ The value of \var{package} must be one o
  \member{SolverOptions.CUSP} -- CUDA sparse linear algebra package\\
  \member{SolverOptions.MKL} -- Intel MKL direct solver\\
  \member{SolverOptions.PASO} -- built-in PASO solver library\\
- \member{SolverOptions.TRILINOS} -- Trilinos solver package (chapter \Refe{TRILINOS})\\
+ \member{SolverOptions.TRILINOS} -- Trilinos solver package (chapter \Refe{Trilinos})\\
  \member{SolverOptions.UMFPACK} -- direct solver from the UMFPACK library.\\
 Not all packages are supported on all implementations. An exception may be
 thrown on some platforms if a particular package is requested.
@@ -959,12 +959,12 @@ minimal residual method\index{linear sol
 \end{memberdesc}
 
 \begin{memberdesc}[SolverOptions]{ROWSUM_LUMPING}
-row sum lumping of the stiffness matrix, see Section~\Refe{LUMPING} for details\index{linear solver!row sum lumping}\index{row sum lumping}.
+row sum lumping of the stiffness matrix, see  for details\index{linear solver!row sum lumping}\index{row sum lumping}.
 Lumping does not use the linear system solver library.
 \end{memberdesc}
 
 \begin{memberdesc}[SolverOptions]{HRZ_LUMPING}
-HRZ lumping of the stiffness matrix, see Section~\Refe{LUMPING} for details\index{linear solver!HRZ lumping}\index{HRZ lumping}.
+HRZ lumping of the stiffness matrix, see  for details\index{linear solver!HRZ lumping}\index{HRZ lumping}.
 Lumping does not use the linear system solver library.
 \end{memberdesc}
 
Index: python-escript-5.6/doc/cookbook/escpybas.tex
===================================================================
--- python-escript-5.6.orig/doc/cookbook/escpybas.tex
+++ python-escript-5.6/doc/cookbook/escpybas.tex
@@ -35,5 +35,5 @@ in this tutorial \esys also includes the
 \item \modpycad  is a package for creating irregular shaped domains.
 \end{enumerate}
 Further explanations of each of these are available in the \esc user guide or in the API documentation\footnote{Available from \url{https://launchpad.net/escript-finley/+download}}. 
-\esc is also dependent on a few other open-source packages which are not maintained by the \esc development team. These are \modnumpy (an array and matrix handling package), \modmpl \footnote{\modnumpy and \modmpl are part of the SciPy package, see \url{http://www.scipy.org/}} (a simple plotting tool) and \verb gmsh \footnote{See \url{http://www.geuz.org/gmsh/}} (which is required by \modpycad). These packages (\textbf{except} for \verb gmsh ) are included with the support bundles. 
+\esc is also dependent on a few other open-source packages which are not maintained by the \esc development team. These are \modnumpy (an array and matrix handling package), \modmpl \footnote{\modnumpy and \modmpl are part of the SciPy package, see \url{http://www.scipy.org/}} (a simple plotting tool) and  gmsh \footnote{See \url{http://www.geuz.org/gmsh/}} (which is required by \modpycad). These packages (\textbf{except} for gmsh ) are included with the support bundles. 
 
Index: python-escript-5.6/doc/cookbook/example08.tex
===================================================================
--- python-escript-5.6.orig/doc/cookbook/example08.tex
+++ python-escript-5.6/doc/cookbook/example08.tex
@@ -160,7 +160,7 @@ y=source[0]*(cos(length(x-xc)*3.1415/src
 src_dir=numpy.array([0.,1.]) # defines direction of point source as down
 y=y*src_dir
 \end{python}
-where \verb xc  is the source point on the boundary of the model. Note that
+where \verb|xc|  is the source point on the boundary of the model. Note that
 because the source is specifically located on the boundary, we have used the
 \verb!FunctionOnBoundary! call to ensure the nodes used to define the source
 are also located upon the boundary. These boundary nodes are passed to
Index: python-escript-5.6/doc/cookbook/example01.tex
===================================================================
--- python-escript-5.6.orig/doc/cookbook/example01.tex
+++ python-escript-5.6/doc/cookbook/example01.tex
@@ -312,7 +312,7 @@ coefficients are interpreted in \esc can
 
 There are various ways to construct domain objects. The simplest form is a
 rectangular shaped region with a length and height. There is
-a ready to use function for this named \verb rectangle(). Besides the spatial
+a ready to use function for this named \verb|rectangle()|. Besides the spatial
 dimensions this function requires to specify the number of
 elements or cells to be used along the length and height, see \reffig{fig:fs}.
 Any spatially distributed value 
@@ -566,9 +566,9 @@ it is advisable to make a careful choice
 
 Finally we initialise an iteration loop to solve our PDE for all the time steps
 we specified in the variable section. As the right hand side of the general form
-is dependent on the previous values for temperature \verb T  across the bar this
-must be updated in the loop. Our output at each time step is \verb T  the heat
-distribution and \verb totT  the total heat in the system.
+is dependent on the previous values for temperature \verb|T|  across the bar this
+must be updated in the loop. Our output at each time step is \verb|T|  the heat
+distribution and \verb|totT|  the total heat in the system.
 \begin{python}
 while t < tend:
 	i+=1 #increment the counter
@@ -720,16 +720,16 @@ name or subdirectory name. We can use th
 \verb|i| as part of our filename. The sub-string \verb|%03d| indicates that we
 want to substitute a value into the name; 
 \begin{itemize}
- \item \verb 0  means that small numbers should have leading zeroes;
- \item \verb 3  means that numbers should be written using at least 3 digits;
+ \item \verb|0|  means that small numbers should have leading zeroes;
+ \item \verb|3|  means that numbers should be written using at least 3 digits;
 and
- \item \verb d  means that the value to substitute will be a decimal integer.
+ \item \verb|d|  means that the value to substitute will be a decimal integer.
 \end{itemize}
 
 To actually substitute the value of \verb|i| into the name write \verb|%i| after
 the string.
 When done correctly, the output files from this command will be placed in the
-directory defined by \verb save_path  as;
+directory defined by \verb|save_path|  as;
 \begin{verbatim}
 blockspyplot001.png
 blockspyplot002.png
@@ -742,7 +742,7 @@ A sub-folder check/constructor is availa
 \begin{verbatim}
 mkDir(save_path)
 \end{verbatim}
-will check for the existence of \verb save_path  and if missing, create the
+will check for the existence of \verb|save_path|  and if missing, create the
 required directories.
 
 We start by modifying our solution script.
